import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class test {
    static String example = "{\"details\" : { \"extra\" : {\"name\" : \"duco\", \"2e\" : \"Lindner\", \"bool\" : false, \"list\" : [{\"test\" : \"Cool\"}, {\"test2\" : \"Cool2\"}]}}}";

    public static void main(String[] args) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;

        try {
            jsonObject = (JSONObject) jsonParser.parse(example);

            System.out.println(getString(jsonObject, "details/extra/name"));
            System.out.println(getBoolean(jsonObject, "details/extra/bool"));
            System.out.println(getList(jsonObject, "details/extra/list"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static String getString(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof String)) {
            throw new ClassCastException();
        }

        return (String) return_value;
    }


    public static boolean getBoolean(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Boolean)) {
            throw new ClassCastException();
        }

        return (boolean) return_value;
    }

    public static List<?> getList(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof List<?>)) {
            throw new ClassCastException();
        }

        return (List<?>) return_value;
    }

    public static double getDouble(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Double)) {
            throw new ClassCastException();
        }

        return (Double) return_value;
    }

    public static int getInt(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Integer)) {
            throw new ClassCastException();
        }

        return (Integer) return_value;
    }
}
