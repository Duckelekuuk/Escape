package com.duckelekuuk.escape.Utils;

import com.duckelekuuk.escape.Escape;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.List;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class JsonConfiguration {

    private File file;
    private JSONObject jsonObject;

    public JsonConfiguration(File file) {
        this.file = file;
        loadFile();
    }

    public JsonConfiguration(String path, String file) {
        this.file = new File(Escape.getInstance().getDataFolder().getAbsolutePath() + "/" + path + "/" + file);
        loadFile();
    }

    public JsonConfiguration(String json) {
        this.file = null;

        JSONParser jsonParser = new JSONParser();

        try {
            this.jsonObject = (JSONObject) jsonParser.parse(json);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
    }

    private void loadFile() {
        JSONParser jsonParser = new JSONParser();

        try {
            this.jsonObject = (JSONObject) jsonParser.parse(new FileReader(file));
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    public boolean save() throws FileNotFoundException {
        try (FileWriter fileWriter = new FileWriter(file)) {

            fileWriter.write(jsonObject.toJSONString());
            fileWriter.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String getString(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof String)) {
            throw new ClassCastException();
        }

        return (String) return_value;
    }


    public static boolean getBoolean(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Boolean)) {
            throw new ClassCastException();
        }

        return (boolean) return_value;
    }

    public static List<?> getList(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof List<?>)) {
            throw new ClassCastException();
        }

        return (List<?>) return_value;
    }

    public static double getDouble(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Double)) {
            throw new ClassCastException();
        }

        return (Double) return_value;
    }

    public static int getInt(JSONObject jsonObject, String path) {
        String[] pieces = path.split("/");

        JSONObject root = jsonObject;
        for (int i = 0; i < pieces.length - 1; i++) {
            root = (JSONObject) root.get(pieces[i]);
        }

        Object return_value = root.get(pieces[pieces.length - 1]);

        if (!(return_value instanceof Integer)) {
            throw new ClassCastException();
        }

        return (Integer) return_value;
    }
}
