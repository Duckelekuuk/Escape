package com.duckelekuuk.escape.Utils;


import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class ItemStackBuilder {

    private ItemStack item;
    private ItemMeta itemMeta;

    public ItemStackBuilder(Material mat) {
        this.item = new ItemStack(mat);
        this.itemMeta = item.getItemMeta();
    }

    public ItemStackBuilder setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public ItemStackBuilder setDurability(short durability) {
        item.setDurability(durability);
        return this;
    }

    @SuppressWarnings("deprecation")
    public ItemStackBuilder setData(DyeColor color) {
        item.getData().setData(color.getData());
        return this;
    }

    public ItemStackBuilder addEnchant(Enchantment enchantment, int level) {
        itemMeta.addEnchant(enchantment, level, true);
        return this;
    }

    public ItemStackBuilder removeEnchant(Enchantment enchantment) {
        itemMeta.removeEnchant(enchantment);
        return this;
    }

    public ItemStackBuilder addItemFlags(ItemFlag... list) {
        itemMeta.addItemFlags(list);
        return this;
    }

    public ItemStackBuilder removeItemFlags(ItemFlag... list) {
        itemMeta.removeItemFlags(list);
        return this;
    }

    public ItemStackBuilder setName(String name) {
        itemMeta.setDisplayName(name);
        return this;
    }

    public ItemStackBuilder setLore(List<String> list) {
        itemMeta.setLore(list);
        return this;
    }

    public ItemStackBuilder setColor(Color color) {
        LeatherArmorMeta meta = (LeatherArmorMeta) itemMeta;
        meta.setColor(color);
        return this;
    }

    public ItemStackBuilder setSkull(String name) {
        SkullMeta meta = (SkullMeta) itemMeta;
        meta.setOwner(name);
        return this;
    }

    public ItemStack build() {
        item.setItemMeta(itemMeta);
        return item;
    }

    @Override
    public String toString() {
        return "Type=" + item.getType().name() + ", Amount=" + item.getAmount() + ", Durability=" + item.getDurability() + ", Enchantments=" + itemMeta.getEnchants() + ", ItemFlags=" + itemMeta.getItemFlags() + ", Name=" + itemMeta.getDisplayName() + ", Lore=" + itemMeta.getLore();
    }
}
