package com.duckelekuuk.escape.Utils;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class TimeUntil {

    public static String formatTime(long time, TimeType type) {
        long first = 0;
        long second = 0;
        switch (type) {
            case HOURS_MINUTES:
                first = time / 3600;
                second = time / 60;
                break;
            case MINUTES_SECONDS:
                first = time / 60;
                second = time % 60;
                break;
            case SECONDS_MILLISECONDS:
                first = time;
                second = first % time;
                break;
        }
        return (first < 10 ? "" : "") + first + ":" + (second < 10 ? "0" : "") + second;
    }

    public enum TimeType {HOURS_MINUTES, MINUTES_SECONDS, SECONDS_MILLISECONDS;}
}
