package com.duckelekuuk.escape.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class LocationHelper {

    public static Location[] getLocationsFromJson(JSONArray jsonArray) {
        Location[] locations = new Location[jsonArray.size()];
        for (int i = 0; i < locations.length; ++i) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            String worldName = (String) jsonObject.get("worldName");
            double x = (double) jsonObject.get("x");
            double y = (double) jsonObject.get("y");
            double z = (double) jsonObject.get("z");
            double yaw = (double) jsonObject.get("yaw");
            double pitch = (double) jsonObject.get("pitch");
            locations[i] = new Location(Bukkit.getWorld(worldName), x, y, z, (float) yaw, (float) pitch);
        }
        return locations;
    }

    public static Location getLocationsFromJson(JSONObject jsonObject) {
        String worldName = (String) jsonObject.get("worldName");
        double x = (double) jsonObject.get("x");
        double y = (double) jsonObject.get("y");
        double z = (double) jsonObject.get("z");
        double yaw = (double) jsonObject.get("yaw");
        double pitch = (double) jsonObject.get("pitch");
        return new Location(Bukkit.getWorld(worldName), x, y, z, (float) yaw, (float) pitch);
    }

    public static JSONObject setLocationToJson(Location location) {
        HashMap<String, Object> locationMap = new HashMap<>();
        locationMap.put("worldName", location.getWorld().getName());
        locationMap.put("x", location.getX());
        locationMap.put("y", location.getY());
        locationMap.put("z", location.getY());
        locationMap.put("yaw", location.getYaw());
        locationMap.put("pitch", location.getPitch());
        return new JSONObject(locationMap);
    }

    @SuppressWarnings("unchecked")
    public static JSONArray setLocationToJson(Location... locations) {
        JSONArray jsonArray = new JSONArray();
        for (Location location : locations) {
            HashMap<String, Object> locationMap = new HashMap<>();
            locationMap.put("worldName", location.getWorld().getName());
            locationMap.put("x", location.getX());
            locationMap.put("y", location.getY());
            locationMap.put("z", location.getY());
            locationMap.put("yaw", location.getYaw());
            locationMap.put("pitch", location.getPitch());
            jsonArray.add(location);
        }
        return jsonArray;
    }
}
