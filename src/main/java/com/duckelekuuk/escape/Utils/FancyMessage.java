package com.duckelekuuk.escape.Utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class FancyMessage {

    private String message;

    public FancyMessage(String message) {
        this.message = "[\"\", { \"text\": \"" + ChatUtils.color(message) + "\"";
    }

    public FancyMessage then(String message) {
        this.message = this.message + "}, { \"text\":\"" + ChatUtils.color(message) + "\"";
        return this;
    }

    public FancyMessage addCommand(String command) {
        this.message = this.message + ", \"clickEvent\": {\"action\":\"run_command\",\"value\":\"" + command + "\"}";
        return this;
    }

    public FancyMessage addSuggestion(String command) {
        this.message = this.message + ", \"clickEvent\": {\"action\":\"suggest_command\",\"value\":\"" + command + "\"}";
        return this;
    }

    public FancyMessage addURL(String url) {
        this.message = this.message + ", \"clickEvent\": {\"action\":\"open_url\",\"value\":\"" + url + "\"}";
        return this;
    }

    public FancyMessage setUnderlined(boolean state) {
        this.message = this.message + ", \"underlined\" : " + state;
        return this;
    }

    public FancyMessage setBold(boolean state) {
        this.message = this.message + ", \"bold\" : " + state;
        return this;
    }

    public FancyMessage setItalic(boolean state) {
        this.message = this.message + ", \"italic\" : " + state;
        return this;
    }

    public FancyMessage setObfuscated(boolean state) {
        this.message = this.message + ", \"obfuscated\" : " + state;
        return this;
    }

    public FancyMessage setStrikethrough(boolean state) {
        this.message = this.message + ", \"strikethrough\" : " + state;
        return this;
    }

    public FancyMessage addToolTip(String... text) {
        String message = "";
        for (int i = 0; i < text.length; i++) {
            if (i == text.length - 1) {
                message = message + text[i];
                continue;
            }
            message = message + text[i] + "\n";
        }


        this.message = this.message + ", \"hoverEvent\": {\"action\":\"show_text\",\"value\":\"" + ChatUtils.color(message) + "\"}";
        return this;
    }

    public void broadcast() {

        for (Player players : Bukkit.getOnlinePlayers()) {
            players.sendRawMessage(this.message + "}]");
        }
    }

    public void send(Player... player) {

        for (Player players : player) {
            players.sendRawMessage(this.message + "}]");
        }
    }

    public String getMessage() {
        return message + "}]";
    }
}
