package com.duckelekuuk.escape.Utils;

import java.util.List;
import java.util.Random;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class RandomHelper {

    private static char[] chars = "abcdefghijklmnopqrstuvwxyz123456789".toCharArray();

    public static String[] toArray(List<String> list) {
        String[] array = new String[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static String getRandomString() {
        return getRandomString(16, true);
    }

    public static String getRandomString(int length) {
        return getRandomString(length, true);
    }

    public static String getRandomString(boolean includeUpperCase) {
        return getRandomString(16, includeUpperCase);
    }

    public static String getRandomString(int length, boolean includeUpperCase) {
        String randomString = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            String randomChar = String.valueOf(random.nextInt(chars.length));
            if (includeUpperCase && !randomChar.matches("[0-9]"))
                randomChar = new Random().nextInt(2) == 0 ? randomChar.toUpperCase() : randomChar;
            randomString += randomChar;
        }
        return randomString;
    }

    public static int randomInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
