package com.duckelekuuk.escape;

import com.duckelekuuk.escape.Managers.CommandManager;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class Escape extends JavaPlugin implements Listener {

    private static Escape instance;
    private CommandManager commandManager;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    public void registerCommands() {

    }

    public static Escape getInstance() {
        return instance;
    }
}
