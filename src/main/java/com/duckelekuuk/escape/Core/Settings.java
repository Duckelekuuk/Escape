package com.duckelekuuk.escape.Core;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class Settings {

    /** PLUGIN SETTINGS **/
    public final static String PLUGIN_NAME = "Escape";
    public final static String PERMISSION = "escape.";
    public final static String PREFIX = "§cEscape §6§b>> §r";

    /** GAME SETTINGS **/
    public final static int MAX_PLAYERS = 24;
    public final static int MIN_PLAYERS = 2;
}
