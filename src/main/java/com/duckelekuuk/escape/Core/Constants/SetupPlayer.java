package com.duckelekuuk.escape.Core.Constants;

import lombok.Getter;

import java.util.UUID;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */

public class SetupPlayer {

    /** PLAYER INFO **/
    @Getter
    private UUID uuid;

    /** SETUP INFO **/


    public SetupPlayer(UUID uuid) {
        this.uuid = uuid;
    }


}
